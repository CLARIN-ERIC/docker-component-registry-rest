#!/bin/bash

# shellcheck source=/project/image/init-scripts.inc.sh
source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/init-scripts.inc.sh"

WEB_INF_DIR="${WEB_APP_BASE_DIR}/WEB-INF"
WEB_XML_TARGET_FILE="${WEB_INF_DIR}/web.xml"

checkVarVal "DOCKER_COMPREG_AUTH_MODE" "${DOCKER_COMPREG_AUTH_MODE}"

WEB_XML_SRC_FILE="";
case "${DOCKER_COMPREG_AUTH_MODE}" in
	"shib")
		WEB_XML_SRC_FILE="web-shib.xml"
		;;
	"test")
		WEB_XML_SRC_FILE="web-test.xml"
		;;
	"read-only")
		WEB_XML_SRC_FILE="web-readonly.xml"
		;;
	*)
		echo "Set DOCKER_COMPREG_AUTH_MODE to 'shib', 'test' or 'read-only'"
		sleep 2
		exit 1
esac

echo "Auth mode '${DOCKER_COMPREG_AUTH_MODE}': activating ${WEB_XML_SRC_FILE}"
makeRestoreBackup "${WEB_XML_TARGET_FILE}"

cp -v "${WEB_INF_DIR}/${WEB_XML_SRC_FILE}" "${WEB_XML_TARGET_FILE}"
