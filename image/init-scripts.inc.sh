#!/bin/bash

export TOMCAT_BASE_DIR="${CATALINA_HOME?error CATALINA_HOME not set}"
export WEB_APP_BASE_DIR="${TOMCAT_BASE_DIR}/webapps/ds#ComponentRegistry"

checkVarVal() {
	if ! [ "$2" ]; then
		echo "Failure: $1 must be set!"
		sleep 2
		exit 1
	fi
}

replaceVarInConfFile() {
	SUBST_VAR=$1
	SUBST_VAL=$2
	TARGET_FILE=$3
	PRINT_VAL=$4
	if [ "${PRINT_VAL}" ]; then
		echo "Filtering ${TARGET_FILE}: {{${SUBST_VAR}}} -> '${PRINT_VAL}'"	
	else
		echo "Filtering ${TARGET_FILE}: {{${SUBST_VAR}}}"
	fi
	replaceVarInFile "${SUBST_VAR}" "${SUBST_VAL}" "${TARGET_FILE}" > /dev/null
}

makeRestoreBackup() {
	if [ -e "${1}.orig" ]; then
		cp "${1}.orig" "${1}"
	else
		cp "${1}" "${1}.orig"
	fi
}
