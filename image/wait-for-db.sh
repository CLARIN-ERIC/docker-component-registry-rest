#!/bin/bash
if ! [ "${DB_URL}" ]; then
	echo "WARNING! Database URL not set. Will NOT check connection before starting service..."
	exit 0
fi


PG_OPTS=$(echo "${DB_URL}"|sed -E 's/^jdbc:postgresql:\/\/(([A-z0-9]|[-_])+):([0-9]+)\/(([A-z0-9]|[-_])+)$/-h \1 -p \3 -d \4/g')
DB_WAIT_TIMEOUT="${DB_WAIT_TIMEOUT:-60}"

echo "Checking connection to ${DB_URL} (timeout: ${DB_WAIT_TIMEOUT}s)"

START_TIME="$(date +%s)"
while ! (echo "${PG_OPTS}" | xargs -t pg_isready); do
	DURATION="$(($(date +%s) - START_TIME))"
	if [ "${DURATION}" -gt "${DB_WAIT_TIMEOUT}" ]; then
		echo "FATAL: Timeout of ${DB_WAIT_TIMEOUT}s has passed"; sleep 1
		exit 1
	fi

	echo "Waiting for database... (pg_isready ${PG_OPTS})"
	sleep 2
done
