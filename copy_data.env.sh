# shellcheck disable=all
COMPREG_VERSION='2.5.1'
REMOTE_RELEASE_URL="https://github.com/clarin-eric/component-registry-rest/releases/download/${COMPREG_VERSION}/component-registry-rest-${COMPREG_VERSION}-docker.tar.gz"
NAME="component-registry-rest-${COMPREG_VERSION}-docker"
WAR_TARGET_DIR='component-registry'

