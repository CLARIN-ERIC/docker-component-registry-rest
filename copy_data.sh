#!/usr/bin/env bash

# shellcheck source=copy_data.env.sh
source "${DATA_ENV_FILE:-copy_data.env.sh}"

init_data () { 
	export INIT_DATA_BUILD_DIR="${PWD}"
	cleanup_data

	DISTR_FILE="distr_$(date +%Y%m%d%H%M%S).tgz"
    echo -n "Fetching remote data from ${REMOTE_RELEASE_URL}... "
    (
		(
			cd webapp &&
			if ! curl --silent --location --output "${DISTR_FILE}" "${REMOTE_RELEASE_URL}"; then
				echo "Failed to retrieve file from ${REMOTE_RELEASE_URL}!"
				exit 1
			fi &&
			tar -zxf "${DISTR_FILE}" &&
			rm "${DISTR_FILE}" &&
			(cd "${NAME}/war" && find . -name '*.war' -exec unzip {} \; > /dev/null) &&
			mv  "${NAME}" "${WAR_TARGET_DIR}"
		) && cd "${INIT_DATA_BUILD_DIR}" || exit 1
    )
}

cleanup_data () {
    if [ -d "${INIT_DATA_BUILD_DIR}/webapp/${WAR_TARGET_DIR}" ]; then
		echo 'Cleaning up data'
	    rm -r "${INIT_DATA_BUILD_DIR}/webapp/${WAR_TARGET_DIR}"
    fi
}
